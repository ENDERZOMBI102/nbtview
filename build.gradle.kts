@file:Suppress("UnstableApiUsage")
plugins {
	id( "org.jetbrains.kotlin.jvm" ) version "1.8.0"
	id( "xyz.wagyourtail.unimined" ) version "1.+"
}

repositories {
	mavenCentral()
	maven( url="https://repo.spongepowered.org/maven" )
	maven( url="https://maven.quiltmc.org/repository/release" )
}

/** Retrieve a value from `gradle.properties` */
fun String.value() =
	ext[this]!! as String


sourceSets.create( "neoforge" )
sourceSets.create( "quilt" )


val cleanLogs by tasks.registering {
	description = "Cleans the logs folder"

	doFirst { // delete all logs
		file( "run/logs" ).listFiles()?.forEach( File::delete )
	}
}

val configurer: xyz.wagyourtail.unimined.api.runs.RunConfig.() -> Unit = {
	workingDir = rootProject.file( "run" )
	runFirst += cleanLogs.get()
	jvmArgs += "-Dlog4j2.configurationFile=${rootProject.file( "log4j2.xml" ).absolutePath}"
	jvmArgs += "-Dforge.logging.markers=REGISTRIES"
	jvmArgs += "-Dforge.logging.console.level=debug"
	jvmArgs += "-Dforge.enabledGameTestNamespaces=${ext["id"]}"
	jvmArgs += "-Dloader.disable_forked_guis=true"
}

java.toolchain.languageVersion.set( JavaLanguageVersion.of( 17 ) )

unimined {
	useGlobalCache = false

	minecraft( sourceSets["main"] ) {
		version( "minecraft_version".value() )

		mappings.intermediary()
		mappings.quilt( 23 )

		runs.off = true
	}

	minecraft( sourceSets["quilt"] ) {
		combineWith( sourceSets["main"] )

		quilt {
			loader( "quilt_version".value() )
		}

		defaultRemapJar = true
		runs.off = false
		runs.config( "client", configurer )
		runs.config( "server", configurer )
	}

	minecraft( sourceSets["neoforge"] ) {
		combineWith( sourceSets["main"] )

		neoForged {
			loader( "neo_version".value() )
		}

		mappings.searge()

		defaultRemapJar = true
		runs.off = false
		runs.config( "client", configurer )
		runs.config( "server", configurer )
	}
}

dependencies {
	compileOnly( "org.spongepowered:mixin:0.8.5-SNAPSHOT" )
	"quiltModImplementation"( "org.quiltmc.quilted-fabric-api:quilted-fabric-api:${"qfapi_version".value()}-${"minecraft_version".value()}" )
}

val props = buildMap {
	val propNames = arrayOf( "id", "title", "license", "version", "group", "description", "repo", "icon" )
	putAll( project.ext.properties.filterKeys { it in propNames || "_version" in it } )
	put( "description", description )
	put( "version", version )
	put( "group", group )
}
tasks.withType<ProcessResources> {
	inputs.properties( props )
	filteringCharset = "UTF-8"

	arrayOf( "fabric.mod.json", "quilt.mod.json", "META-INF/mods.toml", "pack.mcmeta" ).forEach {
		filesMatching( it ) {
			expand( props )
		}
	}
}

tasks.withType<JavaCompile> {
	options.encoding = "UTF-8"
	options.release.set( 17 )
}

tasks.withType<Jar> {
	filesMatching( "LICENSE" ) {
		rename { "${it}_$archiveBaseName"}
	}
}
