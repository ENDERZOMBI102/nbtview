package com.enderzombi102.nbtview

import com.mojang.blaze3d.platform.InputUtil.Type
import net.minecraft.client.option.KeyBind
import net.minecraftforge.api.distmarker.Dist
import net.minecraftforge.api.distmarker.OnlyIn
import net.minecraftforge.client.event.RegisterKeyMappingsEvent
import net.minecraftforge.client.settings.KeyConflictContext
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.event.entity.player.ItemTooltipEvent
import net.minecraftforge.eventbus.api.IEventBus
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.common.Mod
import org.lwjgl.glfw.GLFW.GLFW_KEY_LEFT_ALT

@Mod( Const.ID )
class NbtViewNeoForge( modEventBus: IEventBus ) {
	private val bind by lazy { KeyBind( "key.nbtview.view_nbt", KeyConflictContext.GUI, Type.KEYSYM, GLFW_KEY_LEFT_ALT, "key.categories.misc" ) }

	init {
		modEventBus.register( this )
		MinecraftForge.EVENT_BUS.addListener( this::onCreateTooltip )
		LOGGER.info( "[NbtView] Loaded on (neo)?forge!" )
	}

	@SubscribeEvent
	@OnlyIn( Dist.CLIENT )
	fun onRegisterKeyBind( evt: RegisterKeyMappingsEvent ) {
		evt.register( this.bind )
	}

	@OnlyIn( Dist.CLIENT )
	fun onCreateTooltip( evt: ItemTooltipEvent ) {
		if ( this.bind.isPressed )
			NbtView.onCreateTooltip( evt.itemStack, evt.flags, evt.toolTip )
	}
}
