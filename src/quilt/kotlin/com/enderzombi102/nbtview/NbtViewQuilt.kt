package com.enderzombi102.nbtview


import com.mojang.blaze3d.platform.InputUtil.Type
import net.minecraft.client.item.TooltipContext
import net.minecraft.client.option.KeyBind
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.ItemStack
import net.minecraft.text.Text
import org.lwjgl.glfw.GLFW.GLFW_KEY_LEFT_ALT
import org.quiltmc.loader.api.ModContainer
import org.quiltmc.qsl.base.api.entrypoint.client.ClientModInitializer
import org.quiltmc.qsl.tooltip.api.client.ItemTooltipCallback

class NbtViewQuilt : ClientModInitializer {
	private val bind by lazy { KeyBind( "key.nbtview.view_nbt", Type.KEYSYM, GLFW_KEY_LEFT_ALT, "key.categories.misc" ) }

	override fun onInitializeClient( contaier: ModContainer ) {
		ItemTooltipCallback.EVENT.register { stack: ItemStack, _: PlayerEntity?, context: TooltipContext, lines: MutableList<Text> ->
			if ( bind.isPressed )
				NbtView.onCreateTooltip( stack, context, lines )
		}

		LOGGER.info( "[NbtView] Loaded on quilt!" )
	}
}