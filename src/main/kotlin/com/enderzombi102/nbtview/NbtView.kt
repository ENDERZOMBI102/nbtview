package com.enderzombi102.nbtview

import net.minecraft.client.item.TooltipContext
import net.minecraft.item.ItemStack
import net.minecraft.nbt.NbtHelper
import net.minecraft.text.Text
import org.slf4j.Logger
import org.slf4j.LoggerFactory


@JvmField
val LOGGER: Logger = LoggerFactory.getLogger( "nbtview" )

object NbtView {
	fun onCreateTooltip( stack: ItemStack, context: TooltipContext, lines: MutableList<Text> ) {
		if (! stack.hasNbt() ) {
			lines.add( Text.literal( "{ }" ) )
			return
		}

		val nbt = stack.nbt!!

		lines.removeAt( lines.size - 1 )

		lines.add( NbtHelper.toPrettyPrintedText( nbt ) )
	}
}