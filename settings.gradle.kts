pluginManagement.repositories {
	mavenCentral()
	maven( url="https://maven.wagyourtail.xyz/releases" )
	maven( url="https://maven.wagyourtail.xyz/snapshots" )
	maven( url="https://maven.neoforged.net/releases" )
	maven( url="https://maven.minecraftforge.net" )
	maven( url="https://maven.fabricmc.net" )
	gradlePluginPortal {
		content { excludeGroup( "org.apache.logging.log4j" ) }
	}
}

rootProject.name = "nbtview"
